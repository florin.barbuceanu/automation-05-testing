package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class HovePage extends BasePage {
    @FindBy(xpath = "//button[@class='btn btn-outline-info']")
    WebElement hoverButton;

    @FindBy(id = "result")
    WebElement results;

    public HovePage(WebDriver driver) {
        super(driver);
    }

    public void selectValueFromDropDown(String selectItem) {
//      move to hover button
        actions.moveToElement(hoverButton).build().perform();
//      identify element on the fly
        WebElement itemMenu = driver.findElement(By.id(selectItem));
        System.out.println("Selected item:" + itemMenu.getText());
//      click on element
        itemMenu.click();
    }

    public boolean checkResult(String selectedItem) {
        return results.getText().equals("You last clicked the " + selectedItem);
    }
}
