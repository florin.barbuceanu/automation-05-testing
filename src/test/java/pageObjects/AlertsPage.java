package pageObjects;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AlertsPage extends BasePage{
    WebDriver driver;
    WebDriverWait wait;

    @FindBy(id = "alert-trigger")
    WebElement alertButton;

    @FindBy(id = "confirm-trigger")
    WebElement alertConfButton;

    @FindBy(id = "prompt-trigger")
    WebElement alertPrompButton;

    public AlertsPage(WebDriver driver) {
        super(driver);
    }

    public void showAlertTextAndAccept() {
        alertButton.click();
//      swith to alert
        Alert alert = driver.switchTo().alert();
        System.out.println("Message from alert:" + alert.getText());
//      accept
        alert.accept();
    }

    public void showConfirmationAlertTextAndDismiss() {
        alertConfButton.click();
//      swith to alert
        Alert alert = driver.switchTo().alert();
        System.out.println("Message from alert:" + alert.getText());
//      dismiss
        alert.dismiss();
    }

    public void showPromptAlertTextAndAccept() {
        alertPrompButton.click();
//      swith to alert
        Alert alert = driver.switchTo().alert();
        System.out.println("Message from alert:" + alert.getText());
        alert.sendKeys("Test");
//      accept
        alert.accept();
    }
}
