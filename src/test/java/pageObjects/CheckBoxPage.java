package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CheckBoxPage extends BasePage {

    @FindBy(id = "the_checkbox")
    WebElement checkBox;

    @FindBy(xpath = "//span[@class='off']")
    WebElement checkBoxText;

    public CheckBoxPage(WebDriver driver) {
        super(driver);
    }

    public void selectCheckBox() {
        if (!checkBox.isSelected())
            actions.click(checkBox).build().perform();
    }

    public void unselectCheckBox() {
        if (checkBox.isSelected())
            actions.click(checkBox).build().perform();
    }

    public String getCheckBoxText() {
        return checkBoxText.getText();
    }

    public boolean checkBoxIsSelected() {
        return checkBox.isSelected();
    }
}
