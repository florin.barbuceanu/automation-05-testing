package pageObjects;

import Utils.SeleniumUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {
    WebDriver driver;
    WebDriverWait wait;

    //   define webelementes
//    @FindBy(id = "input-login-username")
    @FindBy(id = "input-login-username")
    WebElement username;

    //@FindBy(id = "input-login-password")
    @FindBy(id = "input-login-password")
    WebElement password;

//    @FindBy(xpath = "//button[@type='submit']")
    @FindBy(xpath = "//button[@class='btn btn-primary']")
    WebElement buttonSubmit;

    @FindBy(xpath = "//input[@id='input-login-username']/parent::div/div[@class='text-left invalid-feedback']")
    WebElement usernameErr;
    @FindBy(xpath = "//input[@id='input-login-password']/parent::div/div[@class='text-left invalid-feedback']")
    WebElement passwordErr;

    By passwordErrBy = By.xpath("//input[@id='input-login-password']/parent::div/div[@class='text-left invalid-feedback']");

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(driver, this);
    }

    public void openLoginPage(String hostname) {
        System.out.println("Open the next url:" + hostname + "/login");
        driver.get(hostname + "/login");
    }

    public void login(String usernameInp, String passwordInput) {
        username.clear();
        username.sendKeys(usernameInp);
        password.clear();
        password.sendKeys(passwordInput);
        buttonSubmit.submit();
    }

    public void waitForPasswordErr() {
        passwordErr = SeleniumUtils.waitForPresenceOfElementGeneric(driver, 15, passwordErrBy);
    }

    public boolean checkErr(String expectedErr, String errorType) {
        if (errorType.equalsIgnoreCase("userErr")) {
            System.out.println("Actual user error:"+usernameErr.getText());
            return expectedErr.equals(usernameErr.getText());
        }
        else if (errorType.equalsIgnoreCase("passErr")) {
//            this.waitForPasswordErr();
            System.out.println("Actual pass error:"+passwordErr.getText());
            return expectedErr.equalsIgnoreCase(passwordErr.getText());
        }
        return false;
    }

}
