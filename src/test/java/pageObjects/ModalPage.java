package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ModalPage extends BasePage{

    @FindBy(xpath = "//button[@class='btn btn-primary']")
    WebElement openModalButton;

    @FindBy(xpath = "//div[@class='modal-footer']/button")
    WebElement closeButton;

    @FindBy(xpath = "//button[@class='close']")
    WebElement closeXButton;

    @FindBy(xpath = "//div[@class='modal-body']")
    WebElement modalBody;


    public ModalPage(WebDriver driver) {
        super(driver);
    }

    public void openModalShowTextAndClose(){
        openModalButton.click();
        System.out.println("Modal text:"+modalBody.getText());
        actions.click(closeButton).build().perform();
    }

    public void openModalShowTextAndCloseX(){
        openModalButton.click();
        System.out.println("Modal text:"+modalBody.getText());
        actions.click(closeXButton).build().perform();
    }
}
