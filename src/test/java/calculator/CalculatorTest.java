package calculator;

import DataProviders.CalculatorDataProviders;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CalculatorTest {
    static Calculator c;

    @BeforeClass
    public static void setUp() {
        c = new Calculator();
    }

    @Test(dataProvider = "calculatorPositiveDataProvider", dataProviderClass = CalculatorDataProviders.class)
    public void testCalculatorPositiveInputs(double el1, double el2, String op, double expectedResults) {
        Assert.assertEquals(c.compute(el1, el2, op), expectedResults, 0.000001);
    }

    @Test
    public void testSumaPositiveNo() {
        System.out.println("Verify that suma:" + c.compute(4, 5, "+") + " is equal with 9 ");
        Assert.assertEquals(9, c.compute(4, 5, "+"), 0);
    }

    @Test
    public void testSumaNegativeNo() {
        Assert.assertEquals(-501, c.compute(-300, -201, "+"), 0);
    }

    @AfterClass
    public static void cleanup() {
        System.out.println("Clean up after tests");
    }
}