package tests;

import Utils.SeleniumUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class RegisterTests extends BaseUITests {
    @Test
    public void registerTests() {
        driver = SeleniumUtils.getDriver(browser);
        driver.get(hostname + "/signup");
        String gender = "Mr";
        WebElement genderElement = SeleniumUtils.waitForPresenceOfElementGeneric(driver, 15, By.xpath("//label[contains(text()," + gender + ")]/parent::div/input"));
        genderElement.click();
        Assert.assertTrue(genderElement.isSelected());
    }
}
