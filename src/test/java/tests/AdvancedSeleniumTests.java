package tests;

import Utils.SeleniumUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;
import pageObjects.AlertsPage;
import pageObjects.CheckBoxPage;
import pageObjects.HovePage;
import pageObjects.ModalPage;

public class AdvancedSeleniumTests extends BaseUITests {

    @Test
    public void alertsTests() {
        driver.get(hostname + "/alert");
        AlertsPage alertsPage = new AlertsPage(driver);
        alertsPage.showAlertTextAndAccept();
        alertsPage.showConfirmationAlertTextAndDismiss();
        alertsPage.showPromptAlertTextAndAccept();
    }

    @Test
    public void hoverTests() {
        driver.get(hostname + "/hover");
        HovePage hovePage = new HovePage(driver);
        hovePage.selectValueFromDropDown("Dog");
        Assert.assertTrue(hovePage.checkResult("Dog"));
    }

    @Test
    public void modalTests() {
        driver.get(hostname + "/modal");
        ModalPage modalPage = new ModalPage(driver);
        modalPage.openModalShowTextAndClose();
        modalPage.openModalShowTextAndCloseX();
    }

    @Test
    public void checkBoxTests() {
        System.out.println("Open the next url:" + hostname + "/interceptor");
        driver.get(hostname + "/interceptor");
        CheckBoxPage checkBoxPage = new CheckBoxPage(driver);
        System.out.println("Select checkBox...");
        checkBoxPage.selectCheckBox();
        System.out.println("Checkbox option: " + checkBoxPage.getCheckBoxText());
        System.out.println("Verify checkbox selected status - expected:true -> actual: " + checkBoxPage.checkBoxIsSelected());
        Assert.assertTrue(checkBoxPage.checkBoxIsSelected());

        System.out.println("Unselect checkbox...");
        checkBoxPage.unselectCheckBox();
        System.out.println("Verify checkbox selected status - expected:false -> actual: " + checkBoxPage.checkBoxIsSelected());
        Assert.assertTrue(checkBoxPage.checkBoxIsSelected());
    }

    @Test
    public void cookiesTests() {
        driver.get(hostname + "/cookie");
        SeleniumUtils.printCookies(driver);
        System.out.println("Set cookie....");
        driver.findElement(By.id("set-cookie")).click();
        System.out.println("-----Show cookies after adding it------");
        SeleniumUtils.printCookies(driver);
    }

    @Test
    public void staleTest() {
        driver.get(hostname + "/stale");
        for (int i = 0; i < 20; i++) {
            WebElement staleButton = driver.findElement(By.id("stale-button"));
            staleButton.click();
        }
    }
}
