package tests;

import Utils.SeleniumUtils;
import io.restassured.RestAssured;
import org.testng.annotations.BeforeClass;

import java.io.IOException;
import java.util.Properties;

public class BaseControllerTest {
    String apiHostname;
    String apiVersion;

    @BeforeClass
    public void setUpApi(){
        try{
            Properties prop = SeleniumUtils.readProperties("src\\test\\resources\\framework.properties");
            apiHostname = prop.getProperty("apiHostName");
            System.out.println("Use next hostname:"+apiHostname);
            apiVersion = prop.getProperty("apiVersion");
            System.out.println("Use next apiVersion:"+apiVersion);

//          set up rest assured based hostname;
            RestAssured.baseURI = apiHostname;
            RestAssured.useRelaxedHTTPSValidation();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
