package tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class MyFirstSeleniumTest {
    WebDriver driver;
    WebDriverWait wait;

    @BeforeClass(alwaysRun = true)
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        WebDriverManager.firefoxdriver().setup();
    }

    @Test
    public void myFistFirefoxDriverTest() {
        driver = new FirefoxDriver();
        driver.get("https://www.google.com/");
        WebElement acceptCookiesButton = driver.findElement(By.xpath("//button[@id='L2AGLb']/div"));
        acceptCookiesButton.click();
        WebElement searchInput = driver.findElement(By.name("q"));
//      scriem in inputul de search
        searchInput.click();
        searchInput.clear();
        searchInput.sendKeys("flori", Keys.ENTER);
//        ne recomandat
//        try {
//            Thread.sleep(5000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//  implicitwait - nerecomandat
//        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
//    explicit wait - recomandat
        wait = new WebDriverWait(driver, 15);
//      afisam titlul site-ului
        System.out.println("Titlu site:" + driver.getTitle());

        List<WebElement> listOfResults = wait.until(
                ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("div.yuRUbf")));
        List<WebElement> promoteListOfResults = wait.until(
                ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("div.v5yQqb")));

        System.out.println("Numar de rezultate gasite:" + (listOfResults.size() + promoteListOfResults.size()));
    }

    @Test
    public void myFistChromeDriverTest() {
        driver = new ChromeDriver();
        driver.get("https://www.google.com/");
        WebElement acceptCookiesButton = driver.findElement(By.xpath("//button[@id='L2AGLb']/div"));
        acceptCookiesButton.click();
        WebElement searchInput = driver.findElement(By.name("q"));
//      scriem in inputul de search
        searchInput.click();
        searchInput.clear();
        searchInput.sendKeys("flori", Keys.ENTER);

//      afisam titlul site-ului
        System.out.println("Titlu site:" + driver.getTitle());

        List<WebElement> listOfResults = driver.findElements(By.cssSelector("div.yuRUbf"));
        List<WebElement> promoteListOfResults = driver.findElements(By.cssSelector("div.v5yQqb"));
        System.out.println("Numar de rezultate gasite:" + (listOfResults.size() + promoteListOfResults.size()));
    }
}
