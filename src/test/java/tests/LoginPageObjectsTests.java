package tests;

import Utils.SeleniumUtils;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.LoginPage;

public class LoginPageObjectsTests extends BaseUITests {

    @Test(dataProvider = "negativeLoginDataProvider", dataProviderClass = LoginTests.class)
    public void testNegativeLogin(String browserType, String username, String password,
                                  String expectedUsernameErr, String expectedPassErr) {
        driver = SeleniumUtils.getDriver(browserType);
        LoginPage loginPage = new LoginPage(driver);

//       open login page
        loginPage.openLoginPage(hostname);

//         login
        loginPage.login(username, password);
        System.out.println("Login button was pressed");

        Assert.assertTrue(loginPage.checkErr(expectedUsernameErr, "userErr"));
        Assert.assertTrue(loginPage.checkErr(expectedPassErr, "passErr"));
    }

    @BeforeMethod(alwaysRun = true)
    public void closeBrowserAfterRun() {
        if (driver != null)
            driver.close();
    }
}
