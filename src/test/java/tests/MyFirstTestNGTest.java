package tests;

import calculator.Calculator;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.Locale;

@Test
public class MyFirstTestNGTest {
    static Calculator c;
    static int count;

    @BeforeClass
    public void setUp() {
        System.out.println("Set up test elements");
        c = new Calculator();
        count = 0;
    }

    @Test(description = "test for edit user account", priority = 2, groups = {"smoke", "regression"})
    public void editAccount() {
        System.out.println("Run test edit user account");
    }

    @Test(description = "method used for login with valid user", priority = 1, groups = {"smoke", "regression"})
    public void loginUserTest() {
        System.out.println("Run login user test");
        Assert.assertFalse(true);
    }

    @Test(enabled = false)
    public void myTestMethod2() {
        System.out.println("Run test method 2");
    }

    @Test(invocationCount = 10, successPercentage = 20)
    public void invocationCountTest() {
        System.out.println("Current invocation count:" + count);
        count++;
        Assert.assertTrue(count < 5);
    }

    @Test(dependsOnMethods = {"loginUserTest"})
    public void testEditAccountDependsOn() {
        System.out.println("Run Edit Account only if Login User Test passed");
    }

    @Test(dependsOnMethods = {"loginUserTest", "editAccount"}, alwaysRun = true, groups = {"regression"})
    public void testLogOut() {
        System.out.println("Log out test");
    }

    public void myTestMethod3() {
        System.out.println("Run test method 3");
    }

    protected void myProtectedTest() {
        System.out.println("Protected Test....");
    }

    @Test(expectedExceptions = {IllegalArgumentException.class})
    public void testException() {
        c.compute(1, 1, "[");
    }

    @AfterClass
    public void cleanUp() {
        System.out.println("Close browser");
    }

    @Test
    @Parameters({"email", "pass"})
    public void loginWithEmailAndPass(String email, String pass) {
        System.out.println("Login with credentials:" + email + "/" + pass);
    }

    // Data providers
    @DataProvider(name = "registerDataProvider")
    public Object[][] registerDataProvider() {
        return new Object[][]{
                {"test@test.com", "Ion Vasile", "Bucuresti"},
                {"test200@new.com", "Maria Popescu", "Ploiesti"},
                {"testing123@me.ro", "Me the booss", "UK"}
        };
    }

    @Test(dataProvider = "registerDataProvider")
    public void registerUser(String email, String name, String adresa) {
        if (name.toLowerCase().contains("maria"))
            Assert.assertTrue(false);
        System.out.println("Register user: {email->" + email + " ,name ->" + name + " adresa->" + adresa + "}");
    }
}
