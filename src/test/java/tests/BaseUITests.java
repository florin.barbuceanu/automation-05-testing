package tests;

import Utils.SeleniumUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import java.io.IOException;
import java.util.Properties;

public class BaseUITests {
    WebDriver driver;
    String hostname;
    String browser;
    String dbHostname, dbUser, dbSchema, dbPassword, dbPort;

    @AfterClass(alwaysRun = true)
    public void close() {
//      inchide browser-ul
        if (driver != null)
            driver.quit();
    }

    @BeforeClass
    public void setUpBasics() throws IOException {
        Properties prop = SeleniumUtils.readProperties("src\\test\\resources\\framework.properties");
//    try to get browser from command line
        browser = System.getProperty("browser");
//      get default value
        if(browser == null)
            browser = prop.getProperty("browser");

        System.out.println("Use browser:" + browser);

        //read default values from config file
        hostname = prop.getProperty("hostname");
        System.out.println("Use next hostname:" + hostname);

//        get DB connection settings
        dbHostname = prop.getProperty("dbHostname");
        System.out.println("Use dbHostname:" + dbHostname);
        dbUser = prop.getProperty("dbUser");
        System.out.println("Use dbUser:" + dbUser);
        dbPort = prop.getProperty("dbPort");
        System.out.println("Use dbPort:" + dbPort);
        dbPassword = prop.getProperty("dbPassword");
        System.out.println("Use dbPassword:" + dbPassword);
        dbSchema = prop.getProperty("dbSchema");
        System.out.println("Use dbSchema:" + dbSchema);

        driver = SeleniumUtils.getDriver(browser);
    }

    protected void enterValuesOnInput(WebElement el, String value) {
        el.click();
        el.clear();
        el.sendKeys(value);
    }

    @AfterMethod
    public void saveScreenShotAtFailure(ITestResult result) {
        if (result.getStatus() == ITestResult.FAILURE) {
            //your screenshooting code goes here
            String testName = result.getMethod().getMethodName();
            System.out.println("Take screen shoot..for test:" + testName);
            SeleniumUtils.takeScreenShot(driver, testName);
        }
    }
}
