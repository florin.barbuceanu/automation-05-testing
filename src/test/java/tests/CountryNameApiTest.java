package tests;

import io.restassured.response.Response;
import org.hamcrest.core.IsEqual;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItem;

public class CountryNameApiTest extends BaseControllerTest {

    @Test
    public void getCountryPartialNameTest() {
        Response response = given().log().uri().
                when().
                get("/{apiVersion}/name/{name}", apiVersion, "rom").
                then().
                statusCode(200).
                contentType(JSON).
                body("name.common[0]", equalTo("Romania")).
                body("currencies[0].RON.symbol", equalTo("lei")).
                extract().response();

        response.getBody().prettyPrint();
    }

    @Test
    public void verifyCountryCapital() {
        given().log().uri().
                when().
                queryParam("fullText", true).
                get("/{apiVersion}/name/{name}", apiVersion, "romania").
                then().
                statusCode(200).
                contentType(JSON).
                body("capital[0][0]", IsEqual.equalTo("Bucharest"));
    }

    @Test
    public void testBorders() {
        List<String> expectedBorders = Arrays.asList("BGR", "HUN", "MDA", "SRB", "UKR");
        List<String> borders = given().log().uri().
                when().
                queryParam("fullText", true).
                get("/{apiVersion}/name/{name}", apiVersion, "romania").
                then().
                statusCode(200).
                contentType(JSON).
//              here it is the magic
        extract().jsonPath().getList("borders[0]", String.class);

        System.out.println("Extracted borders:" + borders);

        Assert.assertTrue(Arrays.equals(borders.toArray(), expectedBorders.toArray()));
    }

    @Test
    public void verifyAltSpellings(){
        given().log().uri().
                when().
                queryParam("fullText", true).
                get("/{apiVersion}/name/{name}", apiVersion, "romania").
                then().
                statusCode(200).
                contentType(JSON).
                body("altSpellings[0]", hasItem("RO")).
                body("altSpellings[0]", hasItem("România"));
    }
}
