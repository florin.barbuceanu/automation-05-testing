package tests;

import Utils.SeleniumUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class LoginTests extends BaseUITests {
//    String url = "http://86.121.249.150:4999/#/login";

    @DataProvider(name = "negativeLoginDataProvider")
    public Object[][] negativeLoginDataProvider() {
        return new Object[][]{
                {"chrome", "", "", "Username is required!", "Password is required!"},
                {"firefox", "test", "", "", "Password is required!"},
                {"edge", "", "123TEst", "Username is required!", ""},
                {"chrome", "test", "TEsting23", "", ""}
        };
    }

    @Test(dataProvider = "negativeLoginDataProvider")
    public void testNegativeLogin(String browserType, String username, String password,
                                  String expectedUsernameErr, String expectedPassErr) {
        driver = SeleniumUtils.getDriver(browserType);
        driver.get(hostname + "/signin");

        WebElement usernameEl = SeleniumUtils.waitForPresenceOfElementGeneric(driver, 15, By.id("input-login-username"));
        WebElement passwordEl = SeleniumUtils.waitForPresenceOfElementGeneric(driver, 15, By.id("input-login-password"));
        WebElement buttonSubmit = SeleniumUtils.waitForPresenceOfElementGeneric(driver, 15, By.xpath("//button[@type='submit']"));

        enterValuesOnInput(usernameEl, username);
        enterValuesOnInput(passwordEl, password);
        buttonSubmit.submit();
        WebElement usernameErr = SeleniumUtils.waitForPresenceOfElementGeneric(driver, 15, By.xpath("//input[@id='input-login-username']/parent::div/div[@class='text-left invalid-feedback']"));
        WebElement passwordErr = SeleniumUtils.waitForPresenceOfElementGeneric(driver, 15, By.xpath("//input[@id='input-login-password']/parent::div/div[@class='text-left invalid-feedback']"));

        Assert.assertEquals(expectedUsernameErr, usernameErr.getText());
        Assert.assertEquals(expectedPassErr, passwordErr.getText());
    }

    @BeforeMethod(alwaysRun = true)
    public void closeBrowserAfterRun() {
        if (driver != null)
            driver.close();
    }
}
