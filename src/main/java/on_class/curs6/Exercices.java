package on_class.curs6;

public class Exercices {

    public static void main(String[] args) {
        Truck truck = new Truck();
        truck.start();
        truck.stop();
        truck.horn();
        truck.lights();
//       -----------------clasa abstracta------------------------
        System.out.println("-----------------clasa abstracta------------------------");
        Bus bus = new Bus();
        bus.turnLeft();
        bus.turnRight();
        bus.reverse();
    }
}
