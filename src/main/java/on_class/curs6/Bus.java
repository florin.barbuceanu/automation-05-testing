package on_class.curs6;

public class Bus extends AbstractCar {

    @Override
    public void turnLeft() {
        System.out.println("Turn left");
    }

    @Override
    public void turnRight() {
        System.out.println("Turn right");
    }

}
