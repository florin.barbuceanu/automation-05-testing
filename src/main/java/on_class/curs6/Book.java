package on_class.curs6;

import java.util.ArrayList;
import java.util.List;

public class Book {
    private List<Integer> prices;
    private String author;

    public Book(String author, List<Integer> prices) {
        this.author = author;
//        bad practice
        this.prices = prices;
//      good solution - copy elements
//        this.prices = new ArrayList<>();
//        for (Integer el : prices)
//            this.prices.add(el);
    }

    public List<Integer> getPrices() {
        return prices;
    }

    public String getAuthor() {
        return author;
    }
}
