package on_class.curs6;

public class Train extends CarInterfaceAbstract {
    @Override
    public void start() {
        System.out.println("Train start...");
    }

    @Override
    public void stop() {
        System.out.println("Train stop..");
    }
}
