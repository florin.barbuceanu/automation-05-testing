package on_class.curs6;

public class Truck implements iCar, iCar2 {

    public void stop() {
        System.out.println("stop() method");
    }

    public void start() {
        System.out.println("start() method");
    }

    public void horn(){
        System.out.println("Beep, beep");
    }

    public void lights(){
        System.out.println("Lights on");
    }
}
