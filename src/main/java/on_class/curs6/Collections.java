package on_class.curs6;

import on_class.curs5.Triangle;

import java.awt.*;
import java.util.*;
import java.util.List;

public class Collections {
    HashMap<String, List<String>> library;

    public void arrays() {
        int[] numbers = {1, 2, 3, 4, 5};
        String[] nume = {"Ion", "Vasile", "Maria"};

        System.out.println("First element:" + numbers[0]);
        nume[nume.length - 1] = "Vasilica";
        System.out.println("Last element:" + nume[nume.length - 1]);

        double[] someDubles = new double[3];
        someDubles[0] = 2.3;
        someDubles[1] = 4.5;
        someDubles[2] = 3.1;

//      dispaly enitre array
        for (int i = 0; i < someDubles.length; i++) {
            System.out.println(someDubles[i]);
        }
//      best way to do
        for (String el : nume) {
            System.out.println(el);
        }

        Triangle[] listOfTriangle = {new Triangle(Color.BLUE), new Triangle(Color.black)};

        for (Triangle tr : listOfTriangle)
            System.out.println(tr);
    }

    public void lists() {
        List<String> myList = new ArrayList<>();
        myList.add("elem1");
        myList.add("ele2");
        myList.add("7");

        System.out.println("Elem poz 2 :" + myList.get(2));
        myList.remove(2);

        System.out.println("List size:" + myList.size());
        for (String el : myList)
            System.out.println(el);
    }

    public void sets() {
//        set contains only non duplicates elements
        Set<Double> mySet = new HashSet<>();
        mySet.add(2.0);
        mySet.add(3.1);
        mySet.add(2.0);

        System.out.println("Size:" + mySet.size());
        mySet.remove(2.0);
        System.out.println("Size aftre remove:" + mySet.size());

    }

    public void maps() {
        Map<String, String> myMap = new HashMap<>();

        myMap.put("Ion Creanga", "Amaintiri din Copilarie");
        myMap.put("Liviu Rebreanu", "Ion");
        myMap.put("Ion Creanga", "Capra cu 3 Iezi");
        myMap.put("New Auth", "Ion");

        System.out.println(myMap.get("Ion Creanga"));

//     display using keys
        System.out.println("Display hashmap using keys");
        for (String key : myMap.keySet()) {
            System.out.println("Author:" + key + " has book:" + myMap.get(key));
        }

        //       remove from map
        myMap.remove("New Auth");

        System.out.println("Display hashmap using values");
        for (String val : myMap.values())
            System.out.println(val);

    }

    public String searchOnList(String searchElem, List<String> list) {
        for (String el : list)
            if (searchElem.equals(el))
                return el;
        return null;
    }

    public boolean searchBook(String name) {
        for (String key : library.keySet()) {
            if (searchOnList(name, library.get(key)) != null) {
                System.out.println("Book with name:" + name + " found");
                return true;
            }
        }
        return false;
    }

    public void listOnMaps() {
        HashMap<String, List<String>> library = new HashMap<>();
        List<String> books1 = new ArrayList<String>(Arrays.asList("Capra cu 3 Iezi",
                "Aminitiri din Copilarie", "Test"));
        List<String> books2 = new ArrayList<String>(Arrays.asList("Test2",
                "Luceafarul", "Somonorase Pasarele"));
//      add elements on map
        library.put("Ion Creanga", books1);
        library.put("Mihai Eminescu", books2);

    }

    public static void main(String[] args) {
        Collections col = new Collections();
        col.arrays();
        col.lists();
        col.sets();
        col.maps();
        List<Integer> prices = new ArrayList<Integer>(
                Arrays.asList(1, 4, 5, 6, 7, 9)
        );
        Book book1 = new Book("Ion Creanga", new ArrayList<Integer>(
                Arrays.asList(1, 4, 5, 6, 7, 9)
        ));
        System.out.println(book1.getPrices());
        prices.add(10);
        System.out.println("Prices:" + prices);
        System.out.println("Book prices:" + book1.getPrices());
    }
}
