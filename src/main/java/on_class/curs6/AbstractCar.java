package on_class.curs6;

import java.awt.*;

abstract public class AbstractCar {
    public Color color;
    public static String nume;

    abstract public void turnLeft();

    abstract public void turnRight();

    protected void reverse() {
        System.out.println("Car reverse!!!!");
    }
}
