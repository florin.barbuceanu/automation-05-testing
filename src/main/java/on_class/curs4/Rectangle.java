package on_class.curs4;

public class Rectangle {

    private double length, width;

    public Rectangle() {
    }

    public Rectangle(double length) {
        this.length = length;
        this.width = length;
    }

    public Rectangle(double length, double width) {
        this.length = length;
        this.width = width;
    }

    public double getArea() {
        return length * width;
    }

    public double getPerimeter() {
        return 2 * (length + width);
    }

    public double getDiagonal() {
        return Math.sqrt(Math.pow(length, 2) + Math.pow(width, 2));
    }

}
