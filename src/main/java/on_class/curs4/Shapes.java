package on_class.curs4;

public class Shapes {
    public static void main(String[] args) {
        Circle myCircle = new Circle(3f);
        myCircle.setRadius(5);
        Circle myCircle2 = new Circle(3f);
        System.out.println("Circle area:" + myCircle.getArea());
        System.out.println("Circle2 area:" + myCircle2.getArea());

//      Square
        Square mySquare = new Square(Integer.parseInt(args[0]));
        System.out.println("Square area:" + mySquare.getArea());
        System.out.println("Square perimeter:" + mySquare.getPerimeter());
        mySquare.displaySquare();

//      rectangle
        Rectangle myReg = new Rectangle(2, 5);
        System.out.println("Rectangle area:" + myReg.getArea());
        System.out.println("Rectangle perimeter:" + myReg.getPerimeter());
        System.out.println("Rectangle diagonal:" + myReg.getDiagonal());

//       Square from rectangle
        Rectangle myRegSquare = new Rectangle(5);
        System.out.println("Rectangle area:" + myRegSquare.getArea());
        System.out.println("Rectangle perimeter:" + myRegSquare.getPerimeter());
        System.out.println("Rectangle diagonal:" + myRegSquare.getDiagonal());
    }
}
