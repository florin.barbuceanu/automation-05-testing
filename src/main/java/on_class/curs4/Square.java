package on_class.curs4;

public class Square {
    private float side;

    //    constructor
    public Square(float side) {
        this.side = side;
    }

    public void setSide(float side) {
        this.side = side;
    }

    public float getArea() {
        return side * side;
    }

    public float getPerimeter() {
        return side * 4;
    }

    public void displaySquare() {
        System.out.println("This is a square with side:" + this.side);
    }
}
