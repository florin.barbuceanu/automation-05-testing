package on_class.curs4;

public class Circle {
    private float radius;
    private static final float PI = 3.141492f;

    public Circle(float radius) {
        this.radius = radius;
    }

    //setter
    public void setRadius(float radius) {
        this.radius = radius;
    }

    public float diameter() {
        return radius * 2;
    }

    public float getArea() {
        return PI * radius * radius;
    }
}
