package on_class.curs4;

public class DrawShapesCornersHomework {

    public static void drawLine(int width) {
        for (int i = 0; i < width; i++) {
            System.out.print("* ");
        }
        System.out.println();
    }

    // draw line:   *              *
    public static void drawLineCorners(int width) {
        for (int i = 0; i < width; i++) {
            if (i == 0 || i == width - 1) {
                System.out.print("* ");
            } else {
                System.out.print("  ");
            }
        }
        System.out.println();
    }

    public static void drawFullShape(int width, int height) {
        for (int i = 0; i < height - 1; i++) {
            drawLine(width);
        }
    }

    public static void drawShapeOutline(int width, int height) {
        for (int i = 0; i < height - 1; i++) {
            if (i == 0 || i == height - 1) {
                drawLine(width);
            } else {
                drawLineCorners(width);
            }
        }
    }

    public static void drawShapeCorners(int width, int height) {
        for (int i = 0; i < height; i++) {
            if (i == 0 || i == height - 1) {
                drawLineCorners(width);
            } else {
                System.out.println();
            }
        }
    }

    public static void main(String[] args) {
        System.out.println("Full Shape:");
        drawFullShape(6, 4);
        System.out.println("Shape Outline:");
        drawShapeOutline(6, 4);
        System.out.println("Shape Corners:");
        drawShapeCorners(6, 4);
    }
}

