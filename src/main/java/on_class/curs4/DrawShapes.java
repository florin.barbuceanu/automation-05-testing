package on_class.curs4;

public class DrawShapes {

    public static void drawLine(int m) {
        for (int i = 1; i <= m; i++)
            System.out.print("*");
    }

    public static void drawFullShape(int n, int m) {
        for (int i = 1; i <= n; i++) {
            drawLine(m);
            System.out.println();
        }
    }

    //    display n*n shape
    public static void drawFullShape(int n) {
        drawFullShape(n, n);
    }

    public static void drawFullShape(String n) {
        drawFullShape(Integer.parseInt(n), Integer.parseInt(n));
    }

    public static void drawFullShape(String n, int m) {
        drawFullShape(Integer.parseInt(n), m);
    }

    public static void drawFullShape(int m, String n) {
        drawFullShape(Integer.parseInt(n), m);
    }

    public static void main(String[] args) {
        drawFullShape(6, 9);
        System.out.println("Draw a shape nxn:");
        drawFullShape(6);
        System.out.println("Draw a shape nxn with string:");
        drawFullShape("3");
    }
}
