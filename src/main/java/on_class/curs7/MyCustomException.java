package on_class.curs7;

public class MyCustomException extends Exception {
    public MyCustomException(String message) {
        super("My custom exception message" + message);
    }
    public MyCustomException(String message, int errorCode) {
        super("My custom exception:" + message + "\nerror code:" + errorCode);
    }
}
