package on_class.curs7;

import java.io.FileNotFoundException;
import java.io.IOException;

public class MainException {
    public static double circleArea(double radius) {
        if (radius <= 0)
            throw new IllegalArgumentException("Radius need to be positive and bigger than 0");
        return Math.PI * radius * radius;
    }

    public static void closeBrowser() {
        System.out.println("close browser");
    }

    public static double divide(double a, double b) throws Exception {
        if (b == 0)
            throw new Exception("divizion by 0 is not supported");
        return (double) a / b;
    }

    public void readFile(String el) throws FileNotFoundException, IOException {
        if (el.length() == 0)
            throw new FileNotFoundException("File not found");
        throw new IOException("Test");
    }

    public static void testMyException(String message, int errorCode) throws MyCustomException {
        throw new MyCustomException(message, errorCode);
    }

    public static void main(String[] args) {
        try {
            double radius = Double.parseDouble(args[0]);
            System.out.println("Aria:" + circleArea(radius));
//            System.out.println(divide(radius, 0));
            testMyException("Test my custom exception", 500);
//      multiple exception line
        } catch (NumberFormatException | ArithmeticException e) {
            System.out.println("Ilegal Number or Arithmetic Exception:" + e.getMessage());
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Index out of bound, pls add paramter value");
        } catch (MyCustomException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            System.out.println("Other exception");
        } finally {
            closeBrowser();
        }
        System.out.println("Am terminat");
    }
}
