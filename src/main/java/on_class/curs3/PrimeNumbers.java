package on_class.curs3;

public class PrimeNumbers {
    public static boolean isPrime(int n) {
        for (int i = 2; i < n / 2; i++)
            if (n % i == 0)
                return false;
        return true;
    }

    public static void displayPrimNumbers(int x, int y) {
        for (int i = x; i <= y; i++)
            if (isPrime(i))
                System.out.println(i);
    }

    public static void main(String[] args) {
        displayPrimNumbers(1, 100);
    }
}
