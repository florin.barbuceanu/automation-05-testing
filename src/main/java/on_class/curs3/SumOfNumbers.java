package on_class.curs3;

public class SumOfNumbers {
    public static int sumOfNumbers(int m, int n) {
        int sum = 0;
        for (int i = m; i <= n; i++)
            sum = sum + i;
        return sum;
    }

    public static void main(String[] args) {
        System.out.println(sumOfNumbers(1, 100));
    }
}
