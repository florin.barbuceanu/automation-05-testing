package on_class.curs3;

public class LeapYear {
    public static boolean isLeapYear(int year) {
        boolean leapYear = false;
        //year divided by 4
        if (year % 4 == 0) {
            if (year % 100 == 0) {
                if (year % 400 == 0)
                    leapYear = true;
            } else leapYear = true;
        }
//        if ((year % 4 == 0 && year % 100 != 0) || (year % 4 == 0 && year % 100 == 0 && year % 400 == 0))
//            return true;
        return leapYear;
    }

    public static void main(String[] args) {
//        year
        int year = Integer.parseInt(args[0]);
        if (year < 1900 || year > 2016) {
            System.out.println("The year is not in the range of 1900-2016");
        } else {
            if (isLeapYear(year)) {
                System.out.println("Year " + year + " has 29 days for February");
            } else {
                System.out.println("Year " + year + " has 28 days for February");
            }
        }
    }
}
