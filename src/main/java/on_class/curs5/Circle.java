package on_class.curs5;

import java.awt.*;

public class Circle extends Shape {
    private float radius;
    private static final float PI = 3.141492f;

    public Circle(Color color) {
        super(color);
    }

    //setter
    public void setRadius(float radius) {
        this.radius = radius;
    }

    public float diameter() {
        return radius * 2;
    }

    public float getArea() {
        return PI * radius * radius;
    }
}
