package on_class.curs5;

public class Light {
    private final int MIN = 0, MAX = 100;
    private int intensity;
    private int delta;

    public Light(int delta) {
        this.delta = delta;
    }

    public void turnON() {
        this.intensity = MAX;
        System.out.println("Light is ON....");
    }

    public void turnOFF() {
        this.intensity = MIN;
        System.out.println("Light is OFF....");
    }

    //delta <= intensity -> if delta > intensity -> 0
    public void dim() {
        if (this.delta <= this.intensity) {
            this.intensity -= this.delta;
        } else {
            this.intensity = 0;
        }
        System.out.println("Intensity after dim: " + this.intensity);
    }

    //    intensity <= MAX - delta -> intensity +=delta
    public void brighten() {
        if (this.intensity <= MAX - delta)
            this.intensity += delta;
        else
            this.intensity = MAX;
        System.out.println("Intensity after brighten: " + this.intensity);
    }

    public void brighten(int nrOfIncrease) {
        for (int i = 1; i <= nrOfIncrease; i++)
            this.brighten();
    }

    public void dim(int nrOfDim) {
        for (int i = 1; i <= nrOfDim; i++)
            this.dim();
    }
}
