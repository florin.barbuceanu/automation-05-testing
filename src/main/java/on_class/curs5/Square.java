package on_class.curs5;

import java.awt.*;

public class Square extends Shape {
    public Square(Color color) {
        super(color);
    }

    @Override
    public void draw() {
        System.out.println("Draw a square....");
    }

    public void showParentAttributes(){
        System.out.println(super.shapeName);
    }
}
