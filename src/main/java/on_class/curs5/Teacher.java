package on_class.curs5;

public class Teacher extends Person {
    private int teacherId;
    private String specializare;

    public Teacher(String nume, int varsta, String cnp, String gen) {
        super(nume, varsta, cnp, gen);
    }

    public Teacher(String nume, int varsta, String cnp, String gen, int teacherId, String specializare) {
        super(nume, varsta, cnp, gen);
        this.specializare = specializare;
        this.teacherId = teacherId;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }

    public String getSpecializare() {
        return specializare;
    }

    public void setSpecializare(String specializare) {
        this.specializare = specializare;
    }

    @Override
    public String toString() {
        return "Teacher - " +
                "teacherId='" + teacherId + '\'' +
                ", specializare='" + specializare + '\'' +
                ", " + super.toString().replace("Person-", "");
    }
}
