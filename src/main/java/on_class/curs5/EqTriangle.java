package on_class.curs5;

import java.awt.*;

public class EqTriangle extends Triangle {
    public EqTriangle(Color color) {
        super(color);
    }

    public void draw(Color color, int n) {
        System.out.println("Draw triangle with color:" + color.toString() + " number of time:" + n);
    }
}
