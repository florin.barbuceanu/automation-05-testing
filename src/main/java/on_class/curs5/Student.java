package on_class.curs5;

public class Student extends Person {
    private int studentId;
    private String grupa;
    private String specializare;

    public Student(String nume, int varsta, String cnp, String gen) {
        super(nume, varsta, cnp, gen);
    }

    public Student(String nume, int varsta, String cnp, String gen, int studentId,
                   String grupa, String specializare) {
        super(nume, varsta, cnp, gen);
        this.studentId = studentId;
        this.grupa = grupa;
        this.specializare = specializare;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getGrupa() {
        return grupa;
    }

    public void setGrupa(String grupa) {
        this.grupa = grupa;
    }

    public String getSpecializare() {
        return specializare;
    }

    public void setSpecializare(String specializare) {
        this.specializare = specializare;
    }

    @Override
    public String toString() {
        return "Student - " +
                "studentId=" + studentId +
                ", grupa='" + grupa + '\'' +
                ", specializare='" + specializare + '\'' +
                ", " + super.toString().replace("Person-", "");
    }
}
