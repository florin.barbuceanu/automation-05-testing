package on_class.curs5;

import java.awt.*;

public class Main {
    public static void main(String[] args) {
        Light light = new Light(50);
        light.turnON();
        light.dim(3);
        light.brighten(4);
        light.turnOFF();

//        --------------------------Static variable--------------------------------------------------

        Car myDacia = new Car(10f);
        System.out.println("My Dacia:" + myDacia.toString());
        System.out.println("Number of cars:" + Car.nrOfCars);
        Car myBmw = new Car(10f);
        Car myCar3 = new Car(10f);
        System.out.println("My BMW:" + myBmw.toString());
        System.out.println("Number of cars:" + Car.getNrOfCarsStatic());
        Car.setNrOfCars(10);
        System.out.println("Number of cars after set:" + myCar3.getNrOfCars());

//       -------------------------------inheritance----------------------------------------------
        Shape shape = new Shape(Color.BLUE);
        shape.draw();
        Triangle triangle = new Triangle(Color.black);
        triangle.draw();
        Square square = new Square(Color.BLUE);
        square.draw();
//        -----------------------------------Polimorfism--------------------------------------------------------

        Shape shape1 = new Circle(Color.BLUE);
        Shape shape2 = new Square(Color.black);
        Shape shape3 = new EqTriangle(Color.BLUE);
        shape1.draw();
        shape2.draw();
        shape3.erase();

//        --------------------------------------------------------------------------

        Person person = new Person("Ion Popescu", 19, "1909029394857", "M");
        Student student1 = new Student("Vasile Marian", 21, "12131321412412", "M", 123, "CA", "computers");
        Teacher teacher = new Teacher("Gabriel Marian", 61, "146131321412412", "M", 123, "computers");
//        will call by default toString
        System.out.println(person);
        System.out.println(student1);
        System.out.println(teacher);
    }
}
