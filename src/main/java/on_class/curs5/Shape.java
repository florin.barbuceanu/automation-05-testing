package on_class.curs5;

import java.awt.*;

public class Shape {
    private Color color;
    protected String shapeName = "Shape";

    public Shape(Color color) {
        this.color = color;
    }

    public Shape(String shapeName) {
        this.shapeName = shapeName;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void draw() {
        System.out.println("Draw a shape...");
    }

    protected void erase() {
        System.out.println("Erase a shape");
    }
}
