package on_class.curs5;

import java.awt.*;

public class Triangle extends Shape {
    public Triangle(Color color) {
        super(color);
        this.shapeName = "Triangle";
    }

    public void flipVertical() {
        System.out.println("Flip Verical");
    }

    public void flipHorizontal() {
        System.out.println("Flip Horizontal");
    }

    protected void erase() {
        System.out.println("Erase from Triangle");
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "shapeName='" + shapeName + '\'' +
                '}';
    }
}
