package on_class.curs5;

public class Person {
    private String nume;
    private int varsta;
    private String cnp;
    private String gen;

    public Person(String nume, int varsta, String cnp, String gen) {
        this.nume = nume;
        this.varsta = varsta;
        this.cnp = cnp;
        this.gen = gen;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public int getVarsta() {
        return varsta;
    }

    public void setVarsta(int varsta) {
        this.varsta = varsta;
    }

    public String getCnp() {
        return cnp;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }

    public String getGen() {
        return gen;
    }

    public void setGen(String gen) {
        this.gen = gen;
    }

    @Override
    public String toString() {
        return "Person-" +
                "nume='" + nume + '\'' +
                ", varsta=" + varsta +
                ", cnp='" + cnp + '\'' +
                ", gen='" + gen;
    }
}
