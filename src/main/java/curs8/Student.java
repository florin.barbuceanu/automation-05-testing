package curs8;

import java.util.Objects;

public class Student {
    private final int nrCrt;
    private final String nume;
    private final String prenume;
    private final int varsta;


    public Student(int nrCrt, String nume, String prenume, int varsta) {
        this.nrCrt = nrCrt;
        this.nume = nume;
        this.prenume = prenume;
        this.varsta = varsta;
    }

    public int getNrCrt() {
        return nrCrt;
    }

    public String getNume() {
        return nume;
    }

    public String getPrenume() {
        return prenume;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
//      daca comparam 2 obiecte diferite sau daca o este null
        if (o == null || getClass() != o.getClass()) return false;
//     downcast -> din parinte -> copil;
        Student student = (Student) o;
        return student.getNume().equals(this.getNume()) && student.getPrenume().equals(this.getPrenume())
                && (student.getVarsta() == this.getVarsta());
    }

    @Override
    public int hashCode() {
        return Objects.hash(nume, prenume, varsta);
    }

    public int getVarsta() {
        return varsta;
    }

    public static void main(String[] args) {
        Student student1 = new Student(1, "Vasile", "Ion", 20);
        Student student2 = new Student(2, "Vasile", "Ion", 20);

        System.out.println(student1.equals(student2));
        Browser browser = Browser.CHROME;
        switch (browser) {
            case CHROME:
                System.out.println("use CHROME");
                break;
            case IE:
                System.out.println("IE");
                break;
            default:
                System.out.println("Use as default chrome");
        }
    }
}
