package curs8;

public enum Browser {
    CHROME,
    IE,
    SAFARI,
    EDGE,
    FIREFOX
}
